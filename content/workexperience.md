---
title: "Work Experience"
date: "{{ .Date }}"
url: "/workexperience"
draft: false
---

# BlockHub
Around the start of June 2018, I started to work for BlockHub, a
small BlockChain focused startup, and as of the end of September, we
 decided that I would also become a co-owner of BlockHub. 
With BlockHub, I worked on a financial arbitrage system. For this system,
we made use of a multitude of microservices, which were connected to
each other using gRPC. Intial data was gathered through the use of
REST and websocket APIs. The data was saved in a data warehouse and
in a in-memory database, which only contained the most recent data.


# Hackathons
While working at BlockHub, two colleagues and I went to Princeton to 
represent Ark, a blockchain company, at HackPrinceton and 
HackPrinceton High 2018. At these hackathons, we guided students on
potential usages and project ideas centered around blockchains, 
setup of an Ark blockchain and how to use the SDKs and APIs to 
interact with the Ark blockchain.

# Volunteer Work
After high school, I went to volunteer with the Parikrma humanity 
foundation, where I worked as a tutor for students with a learning 
disadvantage.