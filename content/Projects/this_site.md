---
title: "This site"
date: "{{ .Date }}"
url: "/this_site/"
draft: false
---

# My portfolio site
This website was made using [hugo](https://gohugo.io/), 
which is a framework for building websites using Golang's templating engine
and Markdown. The code used for the website can be found on my 
[github](github.com/LaurensKubat) page. All the backgrounds were downloaded
from [toptal.com](www.toptal.com/designers/subtlepatterns/)