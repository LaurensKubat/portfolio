---
title: "Education"
date: "{{ .Date }}"
url: "/Education"
draft: false
---

# Radboud University

After a two year stint in biochemistry, I found that my passion
did not lie in biochemistry but in computing science instead.
Thus I decided that I would switch from biochemistry to 
computing science.
I am currently finishing up my bachelor's degree of computing science,
following the 
[cybersecurity](https://www.ru.nl/english/education/bachelors/computing-science/curriculum-and-courses/second-year-courses/)
track of the Radboud University.
